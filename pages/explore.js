/* eslint-disable no-undef */
import React, { useState, useEffect, useCallback, useContext } from 'react';
import { Unity, useUnityContext, } from 'react-unity-webgl';
import { bid, collect, getById, getOwnerOf, resale } from '../lib/web3Adaptor';
import Web3State from '../lib/Web3State';

const Playground = () => {
    const {
        unityProvider,
        isLoaded,
        unload,
        loadingProgression,
        addEventListener,
        initialisationError,
        requestFullscreen,
        removeEventListener,
        sendMessage,
    } = useUnityContext({
        loaderUrl: "build/Build.loader.js",
        dataUrl: "build/Build.data",
        frameworkUrl: "build/Build.framework.js",
        codeUrl: "build/Build.wasm",
    });


    const { accounts, web3, contract } = useContext(Web3State);

    const [id, setId] = useState(0)

    const get = async () => {
        const data = await getById(id);
        if (data) {
            const owner = await getOwnerOf(id);
            var parseData = {
                'id': id,
                'end_timestamp': data.endTimestamp + "000",
                'highest_bidder': data.bidder,
                'min_bid': (data.bidAmount / 10 ** 18).toFixed(2),
                'owner': owner,
            };
            sendMessage("PlayerArmature", "SetNft", JSON.stringify(parseData));
        }
    }
    useEffect(() => {
        if (id == 0) return;
        let interval = setInterval(async () => {
            get()
        }, 3000)
        return () => clearInterval(interval);
    }, [id])

    useEffect(() => {

        if (accounts.length > 0) {
            console.log("account set")
        }
    }, [accounts])

    useEffect(() => {
        if (isLoaded)
            requestFullscreen()
    }, [isLoaded])


    const handleGetData = async (val) => {
        console.log("handleGetData", val)
        setId(() => val)
    }

    const handleGetUser = () => {
        if (accounts.length > 0) {
            sendMessage("PlayerArmature", "SetUser", accounts[0]);
        }
    }
    const handleWithdraw = (val) => {
        collect(val, accounts[0])
    }
    const handleBid = (val) => {
        console.log(val.id, val.amount, accounts[0])
        bid(val.id, val.amount, accounts[0])
    }

    const handleRemove = () => {
        setId(0)
    }

    const handleResale = (val) => {
        console.log(val);
        resale(val, accounts[0])
    }


    useEffect(() => {
        if (isLoaded) {
            console.log(initialisationError)
            window.sendMessage = sendMessage;
            addEventListener("OnNFTCall", handleGetData);
            addEventListener("OnRemove", handleRemove);
            addEventListener("OnUserReady", handleGetUser);
            addEventListener("OnWithdraw", handleWithdraw);
            addEventListener("OnBid", handleBid);
            addEventListener("OnResale", handleResale);
        }
        return () => {

            console.log('unmounting');
            removeEventListener("OnNFTCall", handleGetData);
            removeEventListener("OnRemove", handleRemove);
            removeEventListener("OnUserReady", handleGetUser)
            removeEventListener("OnWithdraw", handleWithdraw)
            removeEventListener("OnBid", handleBid)
            removeEventListener("OnResale", handleResale)
            unload();
        };
    }, [isLoaded]);

    // We'll round the loading progression to a whole number to represent the
    // percentage of the Unity Application that has loaded.
    const loadingPercentage = Math.round(loadingProgression * 100);

    return (
        <div className="playgame">

            <div className="container">
                {isLoaded === false && (
                    // We'll conditionally render the loading overlay if the Unity
                    // Application is not loaded.
                    <div className="loading-overlay">
                        <p>Loading... ({loadingPercentage}%)</p>
                    </div>
                )}
                {(
                    <Unity

                        className="unity"
                        unityProvider={unityProvider}
                        style={{
                            width: "calc(100%)",
                            height: "calc(100vh - 10px)",
                            overflow: "hidden",
                        }}
                    />
                )}
            </div>
        </div>
    );
}

export default Playground