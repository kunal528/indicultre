pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract INDICultreNFT is ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    address contractAddress;

    constructor() ERC721("INDICultreNFT", "INDIC") {
    }

    function changeMarketplace(address marketplace) public {
        contractAddress = marketplace;
    }

    function mint(string memory tokenURI) public returns (uint) {
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();

        _mint(msg.sender, newItemId);
        _setTokenURI(newItemId, tokenURI);
        approve(contractAddress, newItemId);
        return newItemId;
    }
    
    function getLatestTokenId() public view returns (uint256) {
        return _tokenIds.current();
    }
}

contract INDICultreMarket is ReentrancyGuard {
  struct Auction {
        uint32 tokenId;
        address seller;
        address bidder;
        uint256 bidAmount;
        uint256 endTimestamp;
        address artist;
    }

    mapping(uint256 => Auction) public auctions;
    mapping(address => uint256) public balances;
    address[] public addressList;

    address nftAddress;

    uint256 public constant MINIMUM_BIDDING_PRICE = 0.05 ether;

    uint256 private DURATION = 2 minutes;

    function setNFTContract(address _nftAddress) public {
        nftAddress = _nftAddress;
    }

    function setDuration(uint256 sec) public {
        require(sec > 0, "Duration should be greater than 0");
        DURATION = sec;
    }

    function startAuction(uint32 tokenId) public {
        require(
            msg.sender == ERC721(nftAddress).ownerOf(tokenId) ||
                ERC721(nftAddress).ownerOf(tokenId) == address(this),
            "Sender is not allow to start a auction"
        );

        uint256 price = MINIMUM_BIDDING_PRICE;

        if (auctions[tokenId].bidAmount > 0) {
            price = auctions[tokenId].bidAmount;
        }

        address artist = auctions[tokenId].artist;

        if(artist == address(0)){
            artist = IERC721(nftAddress).ownerOf(tokenId);
        }

        Auction memory auction = Auction(
            tokenId,
            msg.sender,
            address(0),
            price,
            block.timestamp + DURATION,
            artist
        );
        auctions[tokenId] = auction;
    }

    function bid(uint32 tokenId) external payable {
        require(
            block.timestamp < auctions[tokenId].endTimestamp,
            "Auction has been ended"
        );
        require(
            msg.value > auctions[tokenId].bidAmount,
            "Value sent is less than current bid"
        );

        if (auctions[tokenId].bidder != address(0)) {
            bool sent = payable(auctions[tokenId].bidder).send(
                auctions[tokenId].bidAmount
            );
            require(sent, "Failed to send Ether");
        }

        auctions[tokenId].bidder = msg.sender;
        auctions[tokenId].bidAmount = msg.value;
    }

    function collect(uint32 tokenId) external {
        require(msg.sender == auctions[tokenId].bidder, "Not highest bidder");
        require(
            block.timestamp > auctions[tokenId].endTimestamp,
            "Auction is not over yet"
        );

        // calculate 10% royalty amount
        uint256 royaltyAmount = auctions[tokenId].bidAmount / 10;

        // transfer royalty amount to the artist
        bool sent = payable(auctions[tokenId].artist).send(royaltyAmount);
        require(sent, "Failed to send royalty amount");

        ERC721(nftAddress).transferFrom(ERC721(nftAddress).ownerOf(tokenId), msg.sender, tokenId);

        if (!(uint256(balances[msg.sender]) > 0)) {
            addressList.push(msg.sender);
        }

        balances[msg.sender] += auctions[tokenId].bidAmount;
    }
}